import sympy as sp

#Function that returns 
def remove_row_column(matrix, row, col):
    # Make copy of the matrix
    M = matrix.copy()

    #Check that row and column are inside the matrix
    num_rows, num_columns = M.shape
    if(row<0 or row>=num_rows or col<0 or col>=num_columns):
        print("Problem!")
        return 0
    else:
        M.row_del(row)
        M.col_del(col)
        return M

def heaviside(n: int) -> int:
    if(n<0):
        return 0
    else:
        return 1

def kronecker_delta(i: int, j: int) -> int:
    if(i==j):
        return 1
    else:
        return 0

def j_matrix(k: int, Nrep: str, elle: str) -> sp.Matrix:
    # Define an k x k matrix filled with zeroes
    jRk = sp.zeros(k, k)

    # Fill the j matrix with the appropriate values
    NR = sp.symbols(Nrep)
    for row in range(k):
        for col in range(k):
            lRX = sp.symbols( elle + str(row - col + 1) )
            jRk[row, col] = NR*lRX*heaviside(row-col) + (row+1)*kronecker_delta(row, col-1)

    return jRk


def test():
    # Define symbolic variables
    x, y, z = sp.symbols('x y z')

    # Define a symbolic matrix
    A = sp.Matrix([[x, y],
                   [z, x + y]])

    # Print the matrix
    print("Matrix A:")
    print(A)

    # Alternatively, you can print the matrix using pretty printing
    print("\nMatrix A (Pretty Printed):")
    sp.pretty_print(A)

    # Perform some operations on the matrix
    det_A = A.det()  # Compute the determinant of A
    inv_A = A.inv()  # Compute the inverse of A

    # Print the results
    print("Determinant of A:", det_A)
    print("Inverse of A:")
    print(inv_A)

    # Alternatively, you can print the matrix using pretty printing
    print("\nMatrix Inv[A] (Pretty Printed):")
    sp.pretty_print(inv_A)

    a1, a2, a3, a4, a5, a6, a7, a8, a9 = sp.symbols('a1 a2 a3 a4 a5 a6 a7 a8 a9')
    M = sp.Matrix([[a1, a2, a3],
                   [a4, a5, a6],
                   [a7, a8, a9]])
    sp.pretty_print(M)
    
    row = 1
    col = 2
    sp.pretty_print(remove_row_column(M, row, col))
    sp.pretty_print(M)


def main():
    #Run test
    test()

    k = 8
    jRk = j_matrix(k, 'Nr', 'lR')
    sp.pretty_print(jRk)


if __name__ == "__main__":
    main()
